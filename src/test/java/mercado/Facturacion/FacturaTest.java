package mercado.facturacion;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;

import org.junit.Before;
import org.junit.Test;

import mercado.cliente.*;
import mercado.mercado.*;
import mercado.sector.*;

public class FacturaTest {

    private Mercado mercado1;
    private Contrato contrato1;
    private Puesto puesto1;
    private Medidor medidor1;
    private ResponsableMercado responsableMercado1;
    private Cliente empresa1,persona1;
    private Sector sector1;
    
    @Test
    public void crearFactura(){


        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);
        medidor1 = new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        persona1=new Persona("Jose", new BigDecimal("20763562"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 6728392, "Savaleta", LocalDate.parse("1990-12-22"));
        mercado1.agregarCliente(persona1);
        empresa1=new Empresa("Coca Cola", new BigDecimal("20401236521"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4876291, "Coca Cola");
        mercado1.agregarCliente(empresa1);
        contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
        LecturaMedidor lecturaMedidor = new LecturaMedidor(YearMonth.parse("2020-07"),new BigDecimal(184) ,new BigDecimal(19),55);
        mercado1.getSector(1).getPuesto(1).getMedidor().agregarLectura(lecturaMedidor);
        mercado1.agregarContrato(contrato1);
        Factura factura1=new Factura(LocalDate.parse("2020-11-01"),"Puesto1",persona1);
        factura1.agregarConcepto(mercado1.getSector(1).getPuesto(1).getMedidor().getLectura(YearMonth.parse("2020-08")));
        factura1.agregarConcepto(mercado1.buscarContratoxNumero(1));
        mercado1.getLibroFacturas().agregarFactura(factura1);
        assertEquals(Integer.parseInt("1"), mercado1.getLibroFacturas().getCantidadFacturas(),0);
        assertEquals(factura1,mercado1.getLibroFacturas().encontrarFactura(Integer.parseInt("1")));
    }

    @Test
    public void crearFacturayVerificarelPrecio(){
        
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);
        medidor1 = new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        persona1=new Persona("Jose", new BigDecimal("20763562"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 6728392, "Savaleta", LocalDate.parse("1990-12-22"));
        mercado1.agregarCliente(persona1);
        empresa1=new Empresa("Coca Cola", new BigDecimal("20401236521"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4876291, "Coca Cola");
        mercado1.agregarCliente(empresa1);
        contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
        LecturaMedidor lecturaMedidor = new LecturaMedidor(YearMonth.parse("2020-07"),new BigDecimal(184) ,new BigDecimal(19),55);
        medidor1.agregarLectura(lecturaMedidor);
        mercado1.agregarContrato(contrato1);
        Factura factura1=new Factura(LocalDate.parse("2020-11-01"),"Puesto1",persona1);
        factura1.agregarConcepto(mercado1.getSector(1).getPuesto(1).getMedidor().getLectura(YearMonth.parse("2020-08")));
        factura1.agregarConcepto(mercado1.buscarContratoxNumero(1));
        mercado1.getLibroFacturas().agregarFactura(factura1);
        assertEquals(new BigDecimal("8671.00"),mercado1.getLibroFacturas().encontrarFactura(1).getPrecioTotalFactua());
    }
}
