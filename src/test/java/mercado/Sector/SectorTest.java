package mercado.sector;

import org.junit.Before;
import org.junit.Test;
import mercado.mercado.*;
import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import java.time.LocalDate;

public class SectorTest {
    private Puesto puesto1,puesto2,puesto3,puesto4;
    private Medidor medidor1,medidor2,medidor3,medidor4;
    private Mercado mercado1;
    private ResponsableMercado responsableMercado1;
    private Sector sector1;

    

    @Test
    public void CrearSectorcon2puestosyObtenerCantidaddePuestos(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392"));    
        responsableMercado1 = new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Norte",new BigDecimal("15"));
        mercado1.agregarSector(sector1);
        medidor1=new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        medidor2=new Medidor(2);
        puesto2 = new Puesto(20,"Puesto con Baño Privado",true,false,false,false,medidor2,mercado1.getSector(1));
        mercado1.getSector("Norte").agregarPuesto(puesto1);
        mercado1.getSector(1).agregarPuesto(puesto2);
        System.out.print(mercado1.getSector(1).getNumeroSector());
        Integer valorEsperado= 2;
        //verifico cantidad de puestos
        assertEquals(valorEsperado,mercado1.getSector(1).getCantidadPuestos());
        assertEquals(new BigDecimal("5175.00"),mercado1.getSector(1).getPuesto(2).getPrecioPuesto());
    }

    @Before
    public void before(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392"));    
        responsableMercado1 = new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Norte",new BigDecimal("15"));
        mercado1.agregarSector(sector1);
        medidor1=new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        medidor2=new Medidor(2);
        puesto2 = new Puesto(30,"Puesto con Baño Privado",true,false,false,false,medidor2,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        mercado1.getSector(1).agregarPuesto(puesto2);       
        
    }
    
    
    @Test
    public void CrearSectorcon2puestosconunoOcupadoyObternerDisponibles(){
        
        Integer valorEsperado= 2;
        //Verifico cuando puestos ahi disponibles
        assertEquals(valorEsperado,mercado1.getSector(1).getCantidadPuestosDisponibles());
        //verifico cuantos puestos ahi en total
        valorEsperado=2;
        assertEquals(valorEsperado,mercado1.getSector(1).getCantidadPuestos());
    }
    
    @Test
    public void CrearSectorcon2puestosBucarunoyAccederasusAtributos(){
        //verifico acceso a atributos
        assertEquals(true,mercado1.getSector(1).getPuesto(1).getDisponibilidad());
        assertEquals(true,mercado1.getSector(1).getPuesto(2).getDisponibilidad());
        Integer aux1= 30;
        assertEquals(aux1,mercado1.getSector(1).getPuesto(2).getDimension());
    }

    @Test
    public void CrearSectorcon2puestosBucarunoyBorraruno(){
        //agrego 2 puestos y borro uno
        mercado1.getSector(1).borrarPuesto(puesto1);
        //verifico que solo alla 1 puesto en el array
        Integer i=1;
        assertEquals(i,mercado1.getSector(1).getCantidadPuestos());
        //vuelvo a agregarlo
        mercado1.getSector(1).agregarPuesto(puesto1);
        //verifico que ahora alla 2 puestos
        i=2;
        assertEquals(i,mercado1.getSector(1).getCantidadPuestos());
        
    }

    @Test 
    public void CrearSectorcon2puestycambiarDisponibilidadde1DespuesAgrego1masyVeoDisponiblesyTotal(){
        medidor3=new Medidor(3);
        puesto3 = new Puesto(40,"Puesto con Baño Privado",true,false,false,false,medidor3,mercado1.getSector(1));
        medidor4=new Medidor(4);
        puesto4 = new Puesto(40, "Puesto con Baño Privado", true, false, false, false,medidor4, mercado1.getSector(1));
        // agrego 2 puestos, uno no diponible y al restante le cambio la disponibilidad
        mercado1.getSector(1).cambiarDisponibilidadDePuesto(1);
        // verifico que no alla ninguno dispobible y que alla 2 puestos cargados
        Integer i = 1;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestosDisponibles());
        i = 2;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestos());
        // agrego un puesto y verifico que solo este este diponible
        mercado1.getSector(1).agregarPuesto(puesto3);
        i = 2;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestosDisponibles());
        i = 3;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestos());
        // cambio la disponilidad de 2 puestos y verifico cuantos puesto ahi disponibles
        mercado1.getSector(1).cambiarDisponibilidadDePuesto(01);
        mercado1.getSector(1).cambiarDisponibilidadDePuesto(02);
        i = 2;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestosDisponibles());
        i=3;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestos());
        // agrego un puesto y le cambio la disponibilidad a 1 y verifico cuantos ahi en
        // total
        mercado1.getSector(1).cambiarDisponibilidadDePuesto(03);
        mercado1.getSector(1).agregarPuesto(puesto4);
        i=2;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestosDisponibles());
        i = 4;
        assertEquals(i, mercado1.getSector(1).getCantidadPuestos());

    }

    @Test
    public void CrearSectorcon2puestosymodificar1() {
        // modifico Puesto numero 02
        Puesto aux = mercado1.getSector(1).getPuesto(02);
        aux.setDimension(50);
        aux.setCamaraFrigorifica(true);
        aux.setAireAcondicionado(true);
        aux.setTecho(true);
        mercado1.getSector(1).modificarPuesto(02, aux);
        // verifico que este puesto se alla modificado
        assertEquals(aux, mercado1.getSector(1).getPuesto(02));
        // cambio disponibilidad de este puesto y verifico que se alla cambiado de true
        // a false
        mercado1.getSector(1).cambiarDisponibilidadDePuesto(02);
        assertEquals(false, mercado1.getSector(1).getPuesto(02).getDisponibilidad());
    }

    @Test
    public void CrearSectorcon2puestosyobtenerPrecio() {
        assertEquals(new BigDecimal("7475.00"), mercado1.getSector(1).getPuesto(02).getPrecioPuesto());
        assertEquals(new BigDecimal("5175.00"), mercado1.getSector(1).getPuesto(01).getPrecioPuesto());
    }


}
