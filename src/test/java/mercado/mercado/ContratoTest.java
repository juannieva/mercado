package mercado.mercado;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import mercado.cliente.*;
import mercado.sector.*;


public class ContratoTest {
    private Mercado mercado1;
    private Contrato contrato1;
    private Medidor medidor1;
    private Puesto puesto1;
    private ResponsableMercado responsableMercado1;
    private Cliente empresa1,persona1;
    private Sector sector1;
    
    @Test
    public void crearContratoPersonayDarledeAltayLuegoBaja(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);
        medidor1= new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        persona1=new Persona("Jose", new BigDecimal("20763562"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 6728392, "Savaleta", LocalDate.parse("1990-12-22"));
        mercado1.agregarCliente(persona1);
        contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
        mercado1.agregarContrato(contrato1);
        assertEquals((Integer)1, mercado1.getNumeroContratosActivos());
        mercado1.darBajaContrato(1);
        assertEquals((Integer)0, mercado1.getNumeroContratosActivos());
    }

    @Test
    public void crearContratoEmpresayDarledeAltayLuegoBaja(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);
        medidor1 = new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        empresa1=new Empresa("Coca Cola", new BigDecimal("20401236521"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4876291, "Coca Cola");
        mercado1.agregarCliente(empresa1);
        contrato1=new Contrato(empresa1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
        mercado1.agregarContrato(contrato1);
        assertEquals((Integer)1, mercado1.getNumeroContratosActivos());
        mercado1.darBajaContrato(1);
        assertEquals((Integer)0, mercado1.getNumeroContratosActivos());
    }

    @Before
    public void iniciarObjetos(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);
        medidor1 = new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        persona1=new Persona("Jose", new BigDecimal("20763562"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 6728392, "Savaleta", LocalDate.parse("1990-12-22"));
        mercado1.agregarCliente(persona1);
        contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
        mercado1.agregarContrato(contrato1);
        empresa1=new Empresa("Coca Cola", new BigDecimal("20401236521"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4876291, "Coca Cola");
        mercado1.agregarCliente(empresa1);
    }
    @Test
    public void verificarPrecioAlquiler(){
        assertEquals(new BigDecimal("5175.00"), mercado1.buscarContratoxNumero(1).getImporteMensual());
    }
    @Test
    public void verificarEstadoPuesto(){
        assertEquals(false, mercado1.buscarContratoxNumero(1).getPuesto().getDisponibilidad());
        assertEquals(false, mercado1.getSector(1).getPuesto(1).getDisponibilidad());
    }

    @Test
    public void dardeBajaContratoyVerificarDisponibilidadPuesto(){
        mercado1.darBajaContrato(1);
        assertEquals(true, mercado1.buscarContratoxNumero(1).getPuesto().getDisponibilidad());
        assertEquals(true, mercado1.getSector(1).getPuesto(1).getDisponibilidad());
    }

    @Test
    public void dardeAltaContratoyVerificarDisponibilidadPuesto(){
        mercado1.darBajaContrato(1);
        assertEquals(true, mercado1.buscarContratoxNumero(1).getPuesto().getDisponibilidad());
        assertEquals(true, mercado1.getSector(1).getPuesto(1).getDisponibilidad());
    }
    
}
