package mercado.mercado;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import mercado.cliente.*;
import mercado.excepciones.*;
import mercado.facturacion.*;
import mercado.sector.*;

public class MercadoTest {
    private Puesto puesto1,puesto2;
    private Medidor medidor1,medidor2;
    private ResponsableMercado responsableMercado1;
    private Mercado mercado1;
    private Cliente empresa1;
    private Cliente persona1;
    private Sector sector1,sector2,sector3;
    private Contrato contrato1,contrato2;

    
    @Test
    public void CrearSectoryAgregarloaMercado(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);   
        assertEquals((Integer)1,mercado1.getNumerodeSectores());
    }

    @Test public void crearMercadoconResponsabley2Puestos(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392"));
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));
        mercado1.setResponsable(responsableMercado1);  
        sector1=new Sector("Norte",new BigDecimal("15"));
        mercado1.agregarSector(sector1);
        medidor1=new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        medidor2=new Medidor(2);
        puesto2 = new Puesto(20,"Puesto con Baño Privado",true,false,false,false,medidor2,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        mercado1.getSector(1).agregarPuesto(puesto2);
        assertEquals((Integer)2,mercado1.getSector(1).getCantidadPuestos());
    }

    @Test
    public void CrearClientesyAgregarlosaMercado(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);
        medidor1=new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        medidor2=new Medidor(2);
        puesto2 = new Puesto(20,"Puesto con Baño Privado",true,false,false,false,medidor2,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        mercado1.getSector(1).agregarPuesto(puesto2);         
        persona1=new Persona("Jose", new BigDecimal("20763562"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 6728392, "Savaleta", LocalDate.parse("1990-12-22"));
        empresa1=new Empresa("Coca Cola", new BigDecimal("20401236521"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4876291, "Coca Cola");
        mercado1.agregarCliente(persona1);
        mercado1.agregarCliente(empresa1);
        assertEquals((Integer)2, mercado1.getNumerodeClientes());
    }
    
    @Before
    public void initObjects(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392"));    
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        sector2=new Sector("Norte",new BigDecimal(20));
        sector3=new Sector("Este",new BigDecimal(25));
        mercado1.agregarSector(sector1);
        mercado1.agregarSector(sector2);
        mercado1.agregarSector(sector3);
        medidor1=new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        medidor2=new Medidor(2);
        puesto2 = new Puesto(20,"Puesto con Baño Privado",true,false,false,false,medidor2,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        mercado1.getSector(1).agregarPuesto(puesto2);
        persona1=new Persona("Jose", new BigDecimal("20763562"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 6728392, "Savaleta", LocalDate.parse("1990-12-22"));
        empresa1=new Empresa("Coca Cola", new BigDecimal("20401236521"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4876291, "Coca Cola");
        mercado1.agregarCliente(persona1);
        mercado1.agregarCliente(empresa1); 
    }  

        //TEST SECTORES
        @Test
        public void agregarSectoresaMercadoyVerificarqueExiste(){
            assertEquals((Integer)3, mercado1.getNumerodeSectores());
            assertEquals((Integer)2,mercado1.getSector(2).getNumeroSector());
            assertEquals((Integer)3,mercado1.getSector(3).getNumeroSector());
        }
    
        @Test (expected = UbicacionExistenteException.class )
        public void agregarSectorYaExistente(){
            mercado1.agregarSector(sector2);
        }
    
        @Test
        public void agregarSectoresaMercadoyEliminarlosdeUnoenUno(){
            assertEquals((Integer)3, mercado1.getNumerodeSectores());
            mercado1.eliminarSector(mercado1.getSector(3));
            assertEquals((Integer)2, mercado1.getNumerodeSectores());
            mercado1.eliminarSector(mercado1.getSector(2));
            assertEquals((Integer)1, mercado1.getNumerodeSectores());
            mercado1.eliminarSector(mercado1.getSector(1));
            assertEquals((Integer)0, mercado1.getNumerodeSectores());
        }
    
        @Test
        public void eliminarSectorconNumero(){
            mercado1.eliminarSector(mercado1.getSector(1));
            assertEquals((Integer)2, mercado1.getNumerodeSectores());
        }
    
        @Test
        public void eliminarSectorconUbicacion(){
            mercado1.eliminarSector(mercado1.getSector("Sur"));
            assertEquals((Integer)2, mercado1.getNumerodeSectores());
        }
    
        @Test (expected = SectorConUnContratoVigenteException.class)
        public void esperarErroralEliminarSectorqueTienePuestoconContratoVigente(){
            Contrato contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
            mercado1.agregarContrato(contrato1);
            mercado1.eliminarSector(mercado1.getSector(1));
    
        }
    
        
        //TEST CLIENTES     
        
        @Test 
        public void verficar2clientesCargados(){
            assertEquals((Integer)2, mercado1.getNumerodeClientes());
        }   

        @Test
        public void agregarClientesaMercadoyEliminar1(){
            mercado1.eliminarCliente(mercado1.getCliente(new BigDecimal("20763562")));
            assertEquals((Integer)1, mercado1.getNumerodeClientes());
            mercado1.eliminarCliente(mercado1.getCliente(new BigDecimal("20401236521")));
            assertEquals((Integer)0, mercado1.getNumerodeClientes());
        }

        @Test (expected = ClienteDuplicadoException.class)
        public void agregar2VeceslaMismaPersona(){
            mercado1.agregarCliente(persona1);
            mercado1.agregarCliente(persona1);
        }

        @Test (expected = ClienteDuplicadoException.class)
        public void agregar2VeceslaMismaEmpresa(){
            mercado1.agregarCliente(empresa1);
            mercado1.agregarCliente(empresa1);
        }

        @Test
        public void ModificarCliente(){
            Cliente aux1,aux= mercado1.getCliente(1);
            aux1=aux;
            aux.setNombre("Roberto");
            mercado1.modificarCliente(aux1,aux);
            assertEquals("Roberto".toUpperCase(),mercado1.getCliente(1).getNombre());
        }


        //TEST CONTRATO

        @Test
        public void agregarContratoPersona(){
            contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
            mercado1.agregarContrato(contrato1);
            assertEquals((Integer)1, mercado1.getNumeroContratosActivos());
        }

        @Test
        public void agregarContratoEmpresa(){
            contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
            mercado1.agregarContrato(contrato1);
            contrato2=new Contrato(empresa1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(2));
            mercado1.agregarContrato(contrato2);
            assertEquals((Integer)2, mercado1.getNumeroContratosActivos());
            assertEquals(contrato1,mercado1.buscarContratoxNumero(1));
            assertEquals(contrato2,mercado1.buscarContratoxNumero(2));
        }
        @Test
        public void dardeBajaContrato(){
            contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
            mercado1.agregarContrato(contrato1);
            contrato2=new Contrato(empresa1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(2));
            mercado1.agregarContrato(contrato2);
            mercado1.darBajaContrato(1);
            assertEquals((Integer)1, mercado1.getNumeroContratosActivos());
            assertEquals(true, mercado1.getSector(contrato1.getUbicacionSector()).getPuesto(contrato1.getPuesto()).getDisponibilidad());

        }

        //TEST FACTURA

        @Test
        public void crearFactura(){
            contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
            LecturaMedidor lectura= new LecturaMedidor(YearMonth.parse("2020-07"),new BigDecimal(184) ,new BigDecimal(19),55);
            medidor1.agregarLectura(lectura);
            mercado1.agregarContrato(contrato1);
            Factura factura1=new Factura(LocalDate.parse("2020-11-01"),"Puesto1",persona1);
            factura1.agregarConcepto(mercado1.getSector(1).getPuesto(1).getMedidor().getLectura(YearMonth.parse("2020-08")));
            factura1.agregarConcepto(mercado1.buscarContratoxNumero(1));
            mercado1.getLibroFacturas().agregarFactura(factura1);
            assertEquals((Integer)1, mercado1.getLibroFacturas().getCantidadFacturas());
            ArrayList<Factura> facturas= new ArrayList<Factura>();
            facturas.add(factura1);
            assertEquals(facturas,mercado1.getCliente(persona1).getFacturasCliente());
        }
    
}
