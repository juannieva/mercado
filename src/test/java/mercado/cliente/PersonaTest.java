package mercado.cliente;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import mercado.excepciones.ApellidoSoloPermiteLetrasException;
import mercado.excepciones.CuitNoValidoException;
import mercado.excepciones.DniNoValidoException;
import mercado.excepciones.EmailInvalidoException;
import mercado.excepciones.MenordeEdadException;
import mercado.excepciones.NombreSoloPermiteLetrasException;
import mercado.excepciones.NumeroTelefonoNoValidoExeption;
import mercado.excepciones.RazonSocialSoloPermiteLetrasException;
import mercado.mercado.*;
import mercado.sector.*;


public class PersonaTest {
    private Mercado mercado1;
    private Contrato contrato1;
    private Puesto puesto1;
    private ResponsableMercado responsableMercado1;
    private Cliente empresa1;
    private Persona persona1;
    private Sector sector1;
    private Medidor medidor1;

    @Test
    public void crearClientePersonayVerificarEdad(){
        persona1=new Persona("Jose", new BigDecimal("20763511"), "no se donde", "Cordoba", "Josesava@gmail.com", 383,(long)4770021, "Savaleta", LocalDate.parse("1990-12-22"));
        assertEquals((Integer)30,persona1.getEdad());
    }   

    @Test   (expected = NombreSoloPermiteLetrasException.class)
    public void crearClientePersonaconNombreIncorrecto(){
        persona1=new Persona("Jose1", new BigDecimal("20763511"), "no se donde", "Cordoba", "Josesava@gmail.com", 383,(long)4770021, "Savaleta", LocalDate.parse("1990-12-22"));
        assertEquals((Integer)29,persona1.getEdad());
    }   

    @Test   (expected = ApellidoSoloPermiteLetrasException.class)
    public void crearClientePersonaconaApellidoIncorrecto(){
        persona1=new Persona("Jose", new BigDecimal("20763511"), "no se donde", "Cordoba", "Josesava@gmail.com", 383,(long)4770021, "Savaleta1", LocalDate.parse("1990-12-22"));
        assertEquals((Integer)29,persona1.getEdad());
    } 


    @Test (expected = MenordeEdadException.class)
    public void crearClientePersonaMenordeEdad(){
        persona1=new Persona("Jose", new BigDecimal("20763511"), "no se donde", "Cordoba", "Josesava@gmail.com", 383,(long)4770021, "Savaleta", LocalDate.parse("2006-12-22"));
        assertEquals((Integer)2,persona1.getEdad());
    }

    @Test (expected = DniNoValidoException.class)
    public void crearClientePersonayIngresarDniInvalido(){
        persona1=new Persona("Jose", new BigDecimal("207635"), "no se donde", "Cordoba", "Josesava@gmail.com", 383,(long)4770021, "Savaleta", LocalDate.parse("1990-12-22"));
        assertEquals((Integer)2,persona1.getEdad());
    }


    @Test (expected = EmailInvalidoException.class)
    public void crearClientePersonayVerificarEmailInvalido(){
        persona1=new Persona("Jose", new BigDecimal("20763512"), "no se donde", "Cordoba", "Josesavagmail.com", 383, (long) 4770021, "Savaleta", LocalDate.parse("1990-12-22"));
        assertEquals((Integer)1,persona1.getEdad());
    }


    @Test  (expected = NumeroTelefonoNoValidoExeption.class)
    public void crearClientePersonayVerificarTelefonoInvalido(){
        persona1=new Persona("Jose", new BigDecimal("20763512"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 477002, "Savaleta", LocalDate.parse("1990-12-22"));   
        assertEquals((Integer)1,persona1.getEdad());    
    }
    

    @Test
    public void CrearClienteEmpresa(){
        empresa1=new Empresa("Coca Cola", new BigDecimal("20401236521"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4770021, "Coca Cola");
        assertEquals(new BigDecimal("20401236521"), empresa1.getDniocuit());
    }

    
    @Test   (expected = CuitNoValidoException.class)
    public void CrearClienteEmpresayIngresarCuitInvalido(){
        empresa1=new Empresa("Coca Cola", new BigDecimal("2040123652111"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4770021, "Coca Cola");
        assertEquals(new BigDecimal("20401236521"), empresa1.getDniocuit());
    }

    @Test   (expected = RazonSocialSoloPermiteLetrasException.class)
    public void CrearClienteEmpresayIngresarRazonSocialInvalido(){
        empresa1=new Empresa("Coca Cola", new BigDecimal("2040123652111"), "Avenida Ocampo Km3", "Catamarca", "Cocacola_cat@gmail.com", 383,(long) 4770021, "Coca Cola1234");
        assertEquals(new BigDecimal("20401236521"), empresa1.getDniocuit());
    }

   @Before
   public void iniciarObjetos(){
        mercado1= new Mercado("Ramos Generales","Barrio 920vv","MercadoRamoGenerales@gmail.com",4700,new BigDecimal("3834727392")); 
        responsableMercado1=new ResponsableMercado("Marcos","Ganne",new BigDecimal("49273263"),"Calle Sin Nombre 123","matimau@gmail.com",new BigDecimal("3834628122"),LocalDate.parse("1997-12-22"));   
        mercado1.setResponsable(responsableMercado1);
        sector1=new Sector("Sur",new BigDecimal(15));
        mercado1.agregarSector(sector1);
        medidor1 = new Medidor(1);
        puesto1 = new Puesto(20,"Puesto Basico",true,false,false,false,medidor1,mercado1.getSector(1));
        mercado1.getSector(1).agregarPuesto(puesto1);
        persona1=new Persona("Jose", new BigDecimal("20763562"), "no se donde", "Cordoba", "Josesava@gmail.com", 383, (long) 6728392, "Savaleta", LocalDate.parse("1990-12-22"));
        mercado1.agregarCliente(persona1);
   } 

   @Test
   public void verificarQueContratoExisteEnCliente(){
        contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
        mercado1.agregarContrato(contrato1);
        assertEquals(1, mercado1.getCliente(persona1).getContratosCliente().size());
   }
   @Test
   public void verificarContratodadodeBajaenCliente(){
        contrato1=new Contrato(persona1, LocalDate.parse("2020-07-12"), LocalDate.parse("2020-08-12"), responsableMercado1, mercado1.getSector(1).getUbicacion(),mercado1.getSector(1).getPuesto(1));
        mercado1.agregarContrato(contrato1);
        assertEquals(true, mercado1.getCliente(persona1).buscarContratoxNumero(1).getAltaContrato());
        mercado1.darBajaContrato(1);
        assertEquals(false, mercado1.getCliente(persona1).buscarContratoxNumero(1).getAltaContrato());
   }
}
