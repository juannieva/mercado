package mercado.mercado;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import mercado.excepciones.*;




public class ResponsableMercado{
    private String nombre;
    private String apellido;
    private BigDecimal dni;
	private String domicilio;
	private String email;
	private BigDecimal telefono;
	private LocalDate fechaNacimiento;
    private Integer edad;
    
    
    //CONTRUCTORES

    public ResponsableMercado(String nombre,String apellido, BigDecimal dni, String domicilio, String email, BigDecimal telefono,
            LocalDate fechaNacimiento) {
        controlIngresoDni(dni.toString());
        controlIngresoApellido(apellido);
        controlIngresoNombre(nombre);
        this.apellido=apellido;
        this.nombre=nombre;
        this.dni= dni;
        this.domicilio=domicilio;
        this.email= email;
        this.telefono= telefono;
        this.fechaNacimiento=fechaNacimiento;
        calculoEdad();
    }


    //SETTERS
        
    public void setNombre(String nombre) {
        controlIngresoNombre(nombre);
		this.nombre = nombre;
    }
    
    public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
    }
    
    public void setEmail(String email) {
        this.email = email;
	}

    public void setTelefono(String telefono) {
		this.telefono = new BigDecimal(telefono);
    }
    
    public void setDni(String dni) {
        controlIngresoDni(dni.toString());
		this.dni = new BigDecimal(dni);
    }

    public void setFechaNacimiento(String fechaNacimiento){
        this.fechaNacimiento=LocalDate.parse(fechaNacimiento);
        calculoEdad();
    }
 
    public void setApellido(String apellido) {
        controlIngresoApellido(apellido);
        this.apellido = apellido;
    }
    //GETTERS
      
    public String getNombre() {
		return nombre;
	}
	
	public String getDomicilio() {
		return domicilio;
	}
	
	public String getEmail() {
		return email;
	}
	
	public BigDecimal getTelefono() {
		return telefono;
	}

	public BigDecimal getDni() {
		return dni;
    }
    
	public Integer getEdad(){
		return edad;
	}

    public String getApellido() {
        return apellido;
    }
    //OTROS METODOS

    private void calculoEdad(){
		LocalDate hoy = LocalDate.now();
        Period periodo = Period.between(fechaNacimiento,hoy);
        this.edad=periodo.getYears();
        if(getEdad()<17){
            throw new MenordeEdadException();
        }
    }

    private static void controlIngresoDni(String dni){
        Pattern pat= Pattern.compile("^([0-9]{8})$");
        Matcher mat=pat.matcher(dni);
        if(mat.find()==false){
            throw new ErrorIngresoDNIException("DNI Invalido Ingresado");
        }
    }

    private static void controlIngresoApellido(String apellido){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s]{0,25})$");
        Matcher mat=pat.matcher(apellido);
        if(mat.find()==false){
            throw new ApellidoSoloPermiteLetrasException();
        }
    }

    private static void controlIngresoNombre(String nombre){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s]{0,25})$");
        Matcher mat=pat.matcher(nombre);
        if(mat.find()==false){
            throw new NombreSoloPermiteLetrasException();
        }
    }

    
    
}