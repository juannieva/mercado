package mercado.mercado;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import mercado.cliente.*;
import mercado.facturacion.*;
import mercado.excepciones.*;
import mercado.sector.*;



public class Mercado {
    private String nombreMercado;
    private String direccion;
    private String email;
    private Integer codigoPostal;
    private BigDecimal telefono;
    private ResponsableMercado responsable;
    private ArrayList<Contrato> contratos;
    private LibroFacturas libroFacturas;
    private ArrayList<Usuario> usuarios;
    private ArrayList<Cliente> clientes;
    private ArrayList<Sector> sectores;


    public Mercado(String nombreMercado, String direccion, String email,Integer codigoPostal, BigDecimal telefono){
        this.nombreMercado=nombreMercado.toUpperCase();
        this.direccion=direccion.toUpperCase();
        this.email=email.toUpperCase();
        this.codigoPostal=codigoPostal;
        this.telefono=telefono;
        this.contratos = new ArrayList<Contrato>();
        this.sectores = new ArrayList<Sector>();
        this.clientes = new ArrayList<Cliente>();
        this.usuarios = new ArrayList<Usuario>();
        this.libroFacturas= new LibroFacturas(clientes);
        }

    //SETTER

    public void setDireccion(String direccion) {
        this.direccion = direccion.toUpperCase();
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public void setNombreMercado(String nombreMercado) {
        this.nombreMercado = nombreMercado.toUpperCase();
    }
    
    public void setTelefono(BigDecimal telefono) {
		this.telefono = telefono;
    }
    
	public void setResponsable(ResponsableMercado responsable) {
		this.responsable = responsable;
	}

    public void setEmail(String email) {
		this.email = email.toUpperCase();
	}

    //GETTERS

    public String getDireccion() {
        return direccion;
    }
 
    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public String getNombreMercado() {
        return nombreMercado;
    }
   
    public BigDecimal getTelefono() {
		return telefono;
	}

	public ResponsableMercado getResponsable() {
		return responsable;
	}

    public String getEmail() {
		return email;
	}

    public LibroFacturas getLibroFacturas(){
        return libroFacturas;
    }
	

  
    //OTROS METODOS

    //METODOS CONTRATOS
    
    public void agregarContrato(Contrato contrato) {
        for(Contrato aux: contratos){
            if(aux.getPuesto().equals(contrato.getPuesto())){
                throw new PuestoYaAsociadoaContratoException();
            }
            if(getSector(contrato.getUbicacionSector()).getPuesto(contrato.getPuesto().getNumeroPuesto()).getDisponibilidad()==false){
                throw new PuestoSeEncuentraenUncontratoVigenteException();
            }
        }
        getSector(contrato.getUbicacionSector()).getPuesto(contrato.getPuesto().getNumeroPuesto()).setDisponibilidad(false);
        contrato.getPuesto().setDisponibilidad(false);
        Integer aux=contratos.size()+1;
        contrato.setNumContrato(aux);
        contratos.add(contrato);
        getCliente(contrato.getCliente()).agregarContratoaCliente(contrato);
    }

    private void modificarContrato(Contrato contrato, Contrato modificado) {
        contratos.remove(contrato);
        contratos.add(modificado);
    }

    public void darBajaContrato(Integer numContrato){
        Contrato contratoEncontrado=buscarContratoxNumero(numContrato);
        Contrato contratoOriginal=contratoEncontrado;
        contratoEncontrado.setAltaContrato(false);
        getSector(contratoOriginal.getUbicacionSector()).cambiarDisponibilidadDePuesto(contratoOriginal.getPuesto().getNumeroPuesto());
        modificarContrato(contratoOriginal, contratoEncontrado);
        getCliente(contratoOriginal.getCliente()).modificarContrato(contratoOriginal, contratoEncontrado);
    }

    public Contrato buscarContratoxNumero(Integer numContrato) {
        Contrato contratoEncontrado = null;
        for (Contrato aux : contratos) {
            if (aux.getCodigoConcepto().equals(numContrato)) {
                contratoEncontrado = aux;
                break;
            }
        }
        if (contratoEncontrado == null) {
            throw new ContratoNoEncontradoException();
        }
        return contratoEncontrado;
    }

    public ArrayList<Contrato> buscarContratosEnUnPeriodo(LocalDate fechaInicio,LocalDate fechaFinal){
        ArrayList<Contrato> contratosEncontrados = new ArrayList<Contrato>();
        for (Contrato auxiliar : contratos) {
            if (fechaInicio.isBefore(auxiliar.getFechaInicio()) && fechaFinal.isAfter(auxiliar.getFechaFin())) {
                contratosEncontrados.add(auxiliar);
            }
        }
        if (contratosEncontrados.isEmpty()) {
            throw new ContratoNoEncontradoException();
        }
        return contratosEncontrados;
    }

    public Integer getNumeroContratosActivos(){
        Integer aux=0;
        for(Contrato var: contratos){
            if(var.getAltaContrato()==true){
                aux++;
            }
        }
        return aux;
    }

    public ArrayList<Contrato> getContratosdeCliente(Cliente cliente){
        ArrayList<Contrato> contratosCliente= new ArrayList<Contrato>();
        for(Contrato aux: contratos){
            if(aux.getCliente().equals(cliente)){
                contratosCliente.add(aux);
            }
        }
        if(contratosCliente.isEmpty()){
            throw new ClienteNotieneContratosCreados();
        }
        return contratosCliente;
    }

    public Integer getNumContratos(){
        return contratos.size();
    }


    //METODOS SECTORES

    public void agregarSector(Sector sectorAux){
        for (Sector aux : sectores) {
            if (aux.getUbicacion().equals(sectorAux.getUbicacion())) {
                throw new UbicacionExistenteException();
            }
        }
        sectorAux.setNumeroSector(sectores.size()+1);
        sectores.add(sectorAux);
        
    }

    public Sector getSector(Integer numeroSector) {
        Sector sectorEncontrado = null;
        for (Sector aux : sectores) {
            if (numeroSector.equals(aux.getNumeroSector())) {
                sectorEncontrado= aux;
                break;
            }
           
        }
        if (sectorEncontrado == null) {
            throw new SectorNoEncontradoException();
          }
        return sectorEncontrado;
    }
    
    public Sector getSector(String ubicacion) {
        Sector sectorEncontrado = null;
        for (Sector aux : sectores) {
            if (aux.getUbicacion().compareToIgnoreCase(ubicacion)==0) {
                sectorEncontrado= aux;
                break;
            }
        }
        if (sectorEncontrado == null) {
            throw new SectorNoEncontradoException();
        }
        return sectorEncontrado;
    }

    public void modificarSector(Sector sectorOriginal, Sector modificado){
        sectores.remove(sectorOriginal);
        sectores.add(modificado);
    }
    
    public void eliminarSector(Sector sector){
        for(Puesto aux: sector.getArrayPuestos()){
            if(aux.getDisponibilidad()==false){
                throw new SectorConUnContratoVigenteException();
            }

        }
        sectores.remove(sector);
    }

    public Integer getNumerodeSectores(){
        return sectores.size();
    }

    //METODOS CLIENTES

    public void agregarCliente(Cliente cliente){
        for(Cliente aux: clientes){
            if(aux.equals(cliente)){
                throw new ClienteDuplicadoException();
            }
            if(aux.getDniocuit().equals(cliente.getDniocuit())){
                throw new DnioCuitclienteyaExisteException();
            }
            if(aux.getEmail().equals(cliente.getEmail())){
                throw new EmailyaExisteenClienteException();
            }
            if(aux.getNombre().equals(cliente.getNombre())){
                throw new NombreyaExisteException();
            }
        }
        cliente.setNumdeCliente(clientes.size()+1);
        clientes.add(cliente);
    }

    public Cliente getCliente(BigDecimal dniocuit){
        Cliente clienteEncontrado=null;
        for(Cliente aux: clientes){
            if(aux.getDniocuit().equals(dniocuit)){
                clienteEncontrado=aux;
                break;
            }
        }
        if(clienteEncontrado==null){
            throw new ClienteNoEncontradoException();
        }
        return clienteEncontrado;
    }


    public Cliente getCliente(Cliente cliente){
        Cliente clienteEncontrado=null;
        for(Cliente aux: clientes){
            if(aux.equals(cliente)){
                clienteEncontrado=aux;
                break;
            }
        }
        if(clienteEncontrado==null){
            throw new ClienteNoEncontradoException();
        }
        return clienteEncontrado;
    }

    public Cliente getCliente(Integer numCliente){
        Cliente clienteEncontrado=null;
        for(Cliente aux: clientes){
            if(aux.getNumdeCliente().equals(numCliente)){
                clienteEncontrado=aux;
                break;
            }
        }
        if(clienteEncontrado==null){
            throw new ClienteNoEncontradoException();
        }
        return clienteEncontrado;
    }

    public void eliminarCliente(Cliente cliente){
        for(Contrato aux: contratos){
            if((aux.getCliente().equals(cliente))&&(aux.getAltaContrato()==true)){
               throw new ClienteTieneContratoVigente();
                
            }

        }
        clientes.remove(cliente);
    }

    public void modificarCliente(Cliente clienteOriginal, Cliente clienteModificado){
        getCliente(clienteOriginal);
        clientes.remove(clienteOriginal);
        clientes.add(clienteModificado);
    }

    public Integer getNumerodeClientes(){
        return clientes.size();
    }


    //METODOS USUARIO

    public void agregarUsuario(Usuario usuario){
        for(Usuario var: usuarios){
            if(var.getDniEmpleado().equals(usuario.getDniEmpleado())){
                throw new EmpleadoYaExisteException("Empleado con DNI:"+usuario.getDniEmpleado()+" ya Registrado");
            }
            if(var.getNombreUsuario().equals(usuario.getNombreUsuario())){
                throw new UsuarioExistenteException("Usuario :"+usuario.getNombreUsuario()+" ya Registrado");
            }
        }
        usuarios.add(usuario);
    }

    public Usuario getUsuario(String nombreDelUsuario){
        Usuario usuarioEncontrado=null;
        for(Usuario var: usuarios){
            if(var.getNombreUsuario().equals(nombreDelUsuario)){
                usuarioEncontrado=var;
                break;
            }
        }
        if(usuarioEncontrado== null){
            throw new UsuarioInexistenteException("Usuario no encontrado");
        }
        return usuarioEncontrado;
    }

    public void eliminarUsuario(Usuario usuario){
        Usuario usuarioAEliminar=getUsuario(usuario.getNombreUsuario());
        usuarios.remove(usuarioAEliminar);
    }

    public void modificarUsuario(Usuario usuarioOriginal, Usuario usuarioModificado){
        eliminarUsuario(usuarioOriginal);
        agregarUsuario(usuarioModificado);
    }

}
