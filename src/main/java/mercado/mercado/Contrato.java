package mercado.mercado;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

import mercado.cliente.*;
import mercado.facturacion.ConceptoFactura;
import mercado.sector.*;


public class Contrato implements ConceptoFactura {
	private BigDecimal precioAlquiler;
	private LocalDate fechaInicio;
    private LocalDate fechaFin;
	private Integer numContrato;
	private Puesto puesto;
	private Cliente cliente;
	private ResponsableMercado responsable;
	private boolean altaContrato;
	private String ubicacionSector;
	
	//CONTRUCTOR
	public Contrato(Cliente cliente,LocalDate fechaInicio, LocalDate fechaFin, ResponsableMercado responsable,String ubicacionSector,Puesto puesto) {
	this.cliente = cliente;
	this.fechaInicio = fechaInicio;
	this.fechaFin = fechaFin;
	this.responsable=responsable;
	this.ubicacionSector=ubicacionSector.toUpperCase();
	this.puesto=puesto;
	this.altaContrato=true;
	}

	//SETTERS

	public void setNumContrato(Integer numContrato){
		this.numContrato=numContrato;
	}
	
    public void setFechaFin(LocalDate fechaFin) {
		this.fechaFin = fechaFin;
	}


	public void setFechaInicio(LocalDate fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
		

	public void setAltaContrato(boolean altaContrato) {
		this.altaContrato = altaContrato;
	}


	//GETTERS

	public LocalDate getFechaInicio() {
		return fechaInicio;
	}
	

	public LocalDate getFechaFin() {
		return fechaFin;
	}


	public Puesto getPuesto() {
		return puesto;
	}

	public ResponsableMercado getResponsable() {
		return responsable;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public boolean getAltaContrato() {
		return altaContrato;
	}

	public String getUbicacionSector() {
		return ubicacionSector;
	}

	@Override
    public BigDecimal getImporteMensual() {
        BigDecimal importeMensual=getPuesto().getPrecioPuesto();
        return importeMensual;
    }

	public BigDecimal getPrecioAlquiler() {
        BigDecimal monthsBetween=new BigDecimal(ChronoUnit.MONTHS.between(fechaInicio.withDayOfMonth(1),fechaFin.withDayOfMonth(1)));
        precioAlquiler=getImporteMensual().multiply(monthsBetween);
        return precioAlquiler;
    }

	@Override
	public Integer getCodigoConcepto() {
		return numContrato;
	}

	public int getMesesDeDuracion(){
        Period meses = Period.between(fechaInicio,fechaFin);
        int periodo = meses.getMonths();
        return periodo;
    }

	//OTROS METODOS
	    
    
}
