package mercado.mercado;

import mercado.excepciones.*;
import java.util.regex.*;

public class Usuario {
    private String apellidoEmpleado;
    private String nombreEmpleado;
    private String dniEmpleado;
    private String nombreUsuario;
    private String contrasenia;

    public Usuario(String apellidoEmpleado, String nombreEmpleado, String dniEmpleado, String nombreUsuario, String contrasenia) {
        controlIngresoDni(dniEmpleado);
        controlIngresoApellidoEmpleado(apellidoEmpleado);
        controlIngresoNombreEmpleado(nombreEmpleado);
        this.apellidoEmpleado=apellidoEmpleado;
        this.nombreEmpleado = nombreEmpleado;
        this.dniEmpleado = dniEmpleado;
        this.nombreUsuario = nombreUsuario;
        this.contrasenia = contrasenia;
        
    }

    //SETTERS

    public void setApellidoEmpleado(String apellidoEmpleado) {
        controlIngresoApellidoEmpleado(apellidoEmpleado);
        this.apellidoEmpleado = apellidoEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        controlIngresoNombreEmpleado(nombreEmpleado);
        this.nombreEmpleado = nombreEmpleado;
    }
    public void setDniEmpleado(String dniEmpleado) {
        controlIngresoDni(dniEmpleado);
        this.dniEmpleado = dniEmpleado;
    }
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    
    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    //GETTERS

    public String getApellidoEmpleado() {
        return apellidoEmpleado;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }


    public String getDniEmpleado() {
        return dniEmpleado;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }


    //OTROS METODOS

    private static void controlIngresoDni(String dniEmpleado){
        Pattern pat= Pattern.compile("^([0-9]{8})$");
        Matcher mat=pat.matcher(dniEmpleado);
        if(mat.find()==false){
            throw new ErrorIngresoDNIException("DNI Invalido Ingresado");
        }
    }

    private static void controlIngresoApellidoEmpleado(String apellidoEmpleado){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s]{0,25})$");
        Matcher mat=pat.matcher(apellidoEmpleado);
        if(mat.find()==false){
            throw new ApellidoSoloPermiteLetrasException();
        }
    }

    private static void controlIngresoNombreEmpleado(String nombreEmpleado){
        Pattern pat= Pattern.compile("^([a-zA-z\\s\\s]{0,25})$");
        Matcher mat=pat.matcher(nombreEmpleado);
        if(mat.find()==false){
            throw new NombreSoloPermiteLetrasException();
        }
    }
    
    
}
