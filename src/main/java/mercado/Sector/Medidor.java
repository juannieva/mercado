package mercado.sector;

import java.time.YearMonth;
import java.util.ArrayList;

import mercado.excepciones.*;


public class Medidor {
    private Integer codigoMedidor;
    private ArrayList<LecturaMedidor> lecturas;


    public Medidor(Integer codigoMedidor) {
        this.codigoMedidor = codigoMedidor;
        lecturas= new ArrayList<LecturaMedidor>();
    }
    //SETTERS

    public void setCodigoMedidor(Integer codigoMedidor) {
        this.codigoMedidor = codigoMedidor;
    }

    //GETTERS
    public Integer getCodigoMedidor() {
        return codigoMedidor;
    }

  
    public ArrayList<LecturaMedidor> getLecturas() {
        return lecturas;
    }

    //OTROS METODOS

    public void agregarLectura(LecturaMedidor lectura){
        for (LecturaMedidor auxiliar : lecturas) {
            if (auxiliar.equals(lectura)) {
              throw new LecturaExisteException("Lectura ya Existe en el medidor");
            }  
        }
        lecturas.add(lectura);

    }

    public LecturaMedidor getLectura(YearMonth periodoLectura){
        LecturaMedidor lecturaMedidorEncontrada = null;
        for (LecturaMedidor auxiliar : lecturas){
            if(auxiliar.getFinPeriodoConsumo().equals(periodoLectura)){
                lecturaMedidorEncontrada = auxiliar;    
            }
        }
        if (lecturaMedidorEncontrada != null) {
            return lecturaMedidorEncontrada;
            
        }
        throw new LecturaNoExisteException("Lectura Inexistente");
    } 

    public void eliminarLectura(YearMonth periodoLectura){
        LecturaMedidor lecturaAEliminar= getLectura(periodoLectura);
        lecturas.remove(lecturaAEliminar);
    }
    
    public void modificarLectura(LecturaMedidor lectura, LecturaMedidor lecturaNueva){
        eliminarLectura(lectura.getFinPeriodoConsumo());
        agregarLectura(lecturaNueva);
    }

    public Integer getNumLecturas(){
        return lecturas.size();
    }
    
    
}
