package mercado.sector;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class Puesto {
	private Sector sector;
	private Boolean disponibilidad;
    private Integer dimension;
    private String comentarios;
    private Integer numeroPuesto;
    private boolean camaraFrigorifica;
    private boolean techo;
    private boolean aireAcondicionado;
	private boolean banoPrivado;
	private Medidor medidor;
	private BigDecimal precioPuesto;
	

	//CONSTRUCTOR

    public Puesto(Integer dimension, String comentarios, boolean camaraFrigorifica,
			boolean techo, boolean aireAcondicionado, boolean banoPrivado,Medidor medidor,Sector sector) {
		this.dimension = dimension;
		this.comentarios = comentarios.toUpperCase();
		this.camaraFrigorifica = camaraFrigorifica;
		this.techo = techo;
		this.aireAcondicionado = aireAcondicionado;
		this.banoPrivado = banoPrivado;
		this.medidor= medidor;
		this.sector=sector;
		this.disponibilidad=true;
		setPrecioPuesto();
	}

	//Setters

	public void setDisponibilidad(Boolean disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
	
	public void setNumeroPuesto(Integer numeroPuesto) {
		this.numeroPuesto = numeroPuesto;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

	public void setCamaraFrigorifica(boolean camaraFrigorifica) {
		this.camaraFrigorifica = camaraFrigorifica;
	}

	public void setTecho(boolean techo) {
		this.techo = techo;
	}

	public void setAireAcondicionado(boolean aireAcondicionado) {
		this.aireAcondicionado = aireAcondicionado;
	}

	public void setBanoPrivado(boolean banoPrivado) {
		this.banoPrivado = banoPrivado;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public void setMedidor(Medidor medidor) {
		this.medidor = medidor;
	}
	
	private void setPrecioPuesto(){
		BigDecimal precioparcial;
		precioparcial=new BigDecimal(dimension).multiply(sector.getPrecioxMetrodePuesto());
		if(techo==true){
			precioparcial=precioparcial.add((new BigDecimal(dimension).multiply(sector.getPrecioxMetrodeTechodePuesto())));
		}
		if(camaraFrigorifica==true){
			precioparcial=precioparcial.add(sector.getPrecioxCamaraFrigorificadePuesto());
		}
		if(aireAcondicionado==true){
			precioparcial=precioparcial.add(sector.getPrecioxAireAcondicionadodePuesto());
		}
		if(banoPrivado==true){
			precioparcial=precioparcial.add(sector.getPrecioxBanoPrivadodePuesto());
		}
		precioparcial=precioparcial.add((precioparcial.multiply(sector.getInteresxSector())));
		precioparcial=precioparcial.setScale(2,RoundingMode.HALF_UP);
		this.precioPuesto=precioparcial;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	//GETTERS

	public Integer getDimension() {
		return dimension;
	}
	
	public Integer getNumeroPuesto() {
		return numeroPuesto;
	}
		
	public boolean getCamaraFrigorifica() {
		return camaraFrigorifica;
	}

	public boolean getTecho() {
		return techo;
	}

	public boolean getAireAcondicionado() {
		return aireAcondicionado;
	}

	public boolean getBanoPrivado() {
		return banoPrivado;
	}
	
	public String getComentarios() {
		return comentarios;
	}

	public Medidor getMedidor() {
		return medidor;
	}

	public BigDecimal getPrecioPuesto() {
		setPrecioPuesto();
		return precioPuesto;
	}
	public Sector getSector() {
		return sector;
	}

	public Boolean getDisponibilidad() {
		return disponibilidad;
	}

	
	
	
	
}
