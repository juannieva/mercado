package mercado.sector;

import java.util.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

import mercado.excepciones.*;

public class Sector {
    private Integer numeroSector;
    private String ubicacion;
    private ArrayList<Puesto> puestos;
    private BigDecimal precioxMetrodePuesto;
	private BigDecimal precioxMetrodeTechodePuesto;
	private BigDecimal precioxCamaraFrigorificadePuesto;
	private BigDecimal precioxAireAcondicionadodePuesto;
    private BigDecimal precioxBanoPrivadodePuesto;
    private BigDecimal interesxSector;

    //CONSTRUCTORES
    public Sector(String ubicacion,BigDecimal interesxSector) {
        this.ubicacion = ubicacion.toUpperCase();
        this.interesxSector = interesxSector.divide(new BigDecimal("100"));
        this.precioxMetrodePuesto=new BigDecimal("200");
		this.precioxMetrodeTechodePuesto=new BigDecimal("50");
		this.precioxCamaraFrigorificadePuesto=new BigDecimal("500");
		this.precioxBanoPrivadodePuesto=new BigDecimal("300");
        this.precioxAireAcondicionadodePuesto=new BigDecimal("200");
        this.puestos= new ArrayList<Puesto>();
    }

    public Sector(String ubicacion,BigDecimal interesxSector,BigDecimal precioxMetrodePuesto,BigDecimal precioxMetrodeTechodePuesto,
                    BigDecimal precioxCamaraFrigorificadePuesto, BigDecimal precioxBanoPrivadodePuesto,BigDecimal precioxAireAcondicionadodePuesto) {
        this.ubicacion = ubicacion.toUpperCase();
        
        this.interesxSector = interesxSector.divide(new BigDecimal("100").setScale(2, RoundingMode.HALF_EVEN));
        this.precioxMetrodePuesto=precioxMetrodePuesto;
		this.precioxMetrodeTechodePuesto=precioxMetrodeTechodePuesto;
		this.precioxCamaraFrigorificadePuesto=precioxCamaraFrigorificadePuesto;
		this.precioxBanoPrivadodePuesto=precioxBanoPrivadodePuesto;
        this.precioxAireAcondicionadodePuesto=precioxAireAcondicionadodePuesto;
        this.puestos= new ArrayList<Puesto>();
    }

    //SETTERS

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion.toUpperCase();
    }

    public void setNumeroSector(Integer numeroSector) {
        this.numeroSector = numeroSector;
    }

  
    
	public void setPrecioxMetrodePuesto(BigDecimal precioxMetrodePuesto) {
		this.precioxMetrodePuesto = precioxMetrodePuesto;
    }
    
	public void setPrecioxMetrodeTechodePuesto(BigDecimal precioxMetrodeTechodePuesto) {
		this.precioxMetrodeTechodePuesto = precioxMetrodeTechodePuesto;
    }
    
	public void setPrecioxCamaraFrigorificadePuesto(BigDecimal precioxCamaraFrigorificadePuesto) {
		this.precioxCamaraFrigorificadePuesto = precioxCamaraFrigorificadePuesto;
    }
    
    
	public void setPrecioxAireAcondicionadodePuesto(BigDecimal precioxAireAcondicionadodePuesto) {
		this.precioxAireAcondicionadodePuesto = precioxAireAcondicionadodePuesto;
	}

    public void setPrecioxBanoPrivadodePuesto(BigDecimal precioxBanoPrivadodePuesto) {
		this.precioxBanoPrivadodePuesto = precioxBanoPrivadodePuesto;
    }
    
    public void setInteresxSector(BigDecimal interesxSector) {
		interesxSector = interesxSector.divide(new BigDecimal("100").setScale(2, RoundingMode.HALF_EVEN));
    }
    

    //GETTERS
    public String getUbicacion() {
        return ubicacion;
    }
    public Integer getCantidadPuestos() {
        return puestos.size();
    }

    public Integer getCantidadPuestosDisponibles() {
        Boolean i=true;
        Integer cantidadPuestosDisponibles=0;
        for (Puesto x : puestos) {
            if((x.getDisponibilidad()).equals(i)){
                cantidadPuestosDisponibles++;
            }            
        }
        return cantidadPuestosDisponibles;
    }

    public Integer getNumeroSector() {
        return numeroSector;
    }

    public BigDecimal getPrecioxMetrodePuesto() {
		return precioxMetrodePuesto;
	}


	public BigDecimal getPrecioxMetrodeTechodePuesto() {
		return precioxMetrodeTechodePuesto;
	}


	public BigDecimal getPrecioxCamaraFrigorificadePuesto() {
		return precioxCamaraFrigorificadePuesto;
	}



	public BigDecimal getPrecioxAireAcondicionadodePuesto() {
		return precioxAireAcondicionadodePuesto;
	}

	public BigDecimal getPrecioxBanoPrivadodePuesto() {
		return precioxBanoPrivadodePuesto;
    }
    
    public BigDecimal getInteresxSector() {
		return interesxSector;
	}

    public ArrayList<Puesto> getArrayPuestos(){
        return puestos;
    }

    //OTROS METODOS

    
    public void agregarPuesto(Puesto puesto){
        Integer i=(puestos.size()+1);
        puesto.setNumeroPuesto(i);
        puestos.add(puesto);
    }

    public Puesto getPuesto(Integer numeroPuesto){
        Puesto puestoEncontrado=null;
        for (Puesto temp : puestos) {
            if (temp.getNumeroPuesto().equals(numeroPuesto)) {
                puestoEncontrado = temp;
                break;
            }
        }
       if (puestoEncontrado == null) {
          throw new PuestoNoEncontradoException();
       }
        return puestoEncontrado;

    }

    public Puesto getPuesto(Puesto puestoBuscado){
        Puesto puestoEncontrado=null;
        for (Puesto temp : puestos) {
            if (temp==puestoBuscado) {
                puestoEncontrado = temp;
                break;
            }
        }
       if (puestoEncontrado == null) {
          throw new PuestoNoEncontradoException();
       }
        return puestoEncontrado;

    }
 

    public void cambiarDisponibilidadDePuesto(Integer numeroPuesto){
        Puesto puestoaModificar = getPuesto(numeroPuesto);
        Puesto aux = getPuesto(numeroPuesto);
        if(puestoaModificar.getDisponibilidad()==true){
            puestoaModificar.setDisponibilidad(false);
        }else{
            puestoaModificar.setDisponibilidad(true);        
        }
        puestos.remove(aux); 
        puestos.add(puestoaModificar);   
    }
    
    public void modificarPuesto(Integer numPuestoaModificar,Puesto puestoModificado){
        Puesto puestoEncontrado;
        puestoEncontrado = getPuesto(numPuestoaModificar);
        puestos.remove(puestoEncontrado); 
        puestos.add(puestoModificado);               
    }
    
    public void borrarPuesto(Puesto puesto){
        Puesto puestoaBorrar= getPuesto(puesto);
        if(puestoaBorrar.getDisponibilidad()==false){
            throw new PuestoYaAsociadoaContratoException();
        } 
        puestos.remove(puestoaBorrar);             
	}

	  

    }

