package mercado.sector;

import java.math.BigDecimal;
import java.time.YearMonth;

import mercado.facturacion.*;

public class LecturaMedidor implements ConceptoFactura{
    private YearMonth inicioPeriodoConsumo;
    private YearMonth finPeriodoConsumo;
    private BigDecimal consumoMes;
    private BigDecimal preciokWh;
    private Integer codigoLectura;

    public LecturaMedidor(YearMonth inicioPeriodoConsumo,BigDecimal consumoMes,BigDecimal preciokWh, Integer codigoLectura){
        this.consumoMes=consumoMes;
        this.preciokWh=preciokWh;
        this.inicioPeriodoConsumo=inicioPeriodoConsumo;
        this.finPeriodoConsumo=inicioPeriodoConsumo.plusMonths(1);
        this.codigoLectura=codigoLectura;
    }

    //SETTERS
    public void setPreciokWh(BigDecimal preciokWh) {
        this.preciokWh = preciokWh;
    }

    public Integer getCodigoLectura() {
        return codigoLectura;
    }
    public void setInicioPeriodoConsumo(YearMonth inicioPeriodoConsumo) {
        this.inicioPeriodoConsumo = inicioPeriodoConsumo;
    }

    public void setFinPeriodoConsumo(YearMonth finPeriodoConsumo) {
        this.finPeriodoConsumo = finPeriodoConsumo;
    }

    public void setConsumoMensual(BigDecimal consumoMes) {
        this.consumoMes = consumoMes;
    }

    public void setCodigoLectura(Integer codigoLectura) {
        this.codigoLectura = codigoLectura;
    }


    //GETTERS
     public YearMonth getInicioPeriodoConsumo() {
        return inicioPeriodoConsumo;
    }
    public YearMonth getFinPeriodoConsumo() {
        return finPeriodoConsumo;
    } 
    public BigDecimal getConsumoMensual() {
        return consumoMes;
    }
    public BigDecimal getPreciokWh() {
        return preciokWh;
    }
    @Override
     public BigDecimal getImporteMensual() {
        BigDecimal importeMensual = preciokWh.multiply(consumoMes);
        return importeMensual;
    }

    @Override
    public Integer getCodigoConcepto() {
        return codigoLectura;
    }

    
    


    


}
