package mercado.cliente;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mercado.excepciones.*;


public class Persona extends Cliente {

    private String apellido;
    private LocalDate fechaNacimiento;
    private Integer edad;

    //constructor
    public Persona(String nombre, BigDecimal dniocuit, String domicilio,String provincia, String email,Integer codigodeArea, Long telefono, String apellido,LocalDate fechaNacimiento) {
        super(nombre, dniocuit, domicilio,provincia, email,codigodeArea, telefono);
        comprobarDniOcuit(dniocuit);
        comprobarApellido(apellido);
        this.apellido = apellido.toUpperCase();
        this.fechaNacimiento=fechaNacimiento;
        calculoEdad();
    }
    
    //GETTERS
    public String getApellido() {
        return apellido;
    }
    public Integer getEdad() {
        return edad;
    }
    
    //SETTERS
    public void setApellido(String apellido) {
        this.apellido = apellido.toUpperCase();
    }    

    //OTROS METODOS
    private void calculoEdad(){
		LocalDate hoy = LocalDate.now();
        Period periodo = Period.between(fechaNacimiento,hoy);
        this.edad=periodo.getYears();
        comprobarMenorEdad();
    }

	private void comprobarMenorEdad(){
		Integer aux=17;
        if(edad<aux){
            throw new MenordeEdadException();
        }
    }
    
    protected void comprobarDniOcuit(BigDecimal dniocuit){
        Pattern pat = Pattern.compile("^([0-9]{8,8})$");
		Matcher mat = pat.matcher(dniocuit.toString());
		if(mat.find()==false){
			throw new DniNoValidoException();
		 }
    }   

    private void comprobarApellido(String apellido){
		Pattern pat = Pattern.compile("^([a-zA-Z\s\s\s]{0,15})$");
		Matcher mat = pat.matcher(apellido);
		if(mat.find()==false){
			throw new ApellidoSoloPermiteLetrasException();
		 }	
	}
}
