package mercado.cliente;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mercado.excepciones.CuitNoValidoException;
import mercado.excepciones.RazonSocialSoloPermiteLetrasException;

public class Empresa extends Cliente {
    private String razonSocial;
    //CONSTRUCTOR
    public Empresa(String nombre, BigDecimal cuit, String domicilio,String provincia, String email,Integer codigodeArea, Long telefono, String razonSocial) {
        super(nombre, cuit, domicilio,provincia, email,codigodeArea, telefono);
        comprobarRazonSocial(razonSocial);
        this.razonSocial = razonSocial.toUpperCase();
        comprobarDniOcuit(cuit);
    } 
      
    //SETTERS
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial.toUpperCase();
    }

    @Override
    public void setDniocuit(BigDecimal cuit){
        comprobarDniOcuit(cuit);
        super.setDniocuit(cuit);
    }
    //GETTERS
    public String getRazonSocial() {
        return razonSocial;
    }

    //OTROS METODOS
    @Override
    protected void comprobarDniOcuit(BigDecimal dniocuit){
        Pattern pat = Pattern.compile("^([0-9]{11,11})$");
		Matcher mat = pat.matcher(dniocuit.toString());
		if(mat.find()==false){
			throw new CuitNoValidoException();
		 }
    }

    private void comprobarRazonSocial(String nombre){
        Pattern pat = Pattern.compile("^([a-zA-Z\s\s]{0,15})$");
		Matcher mat = pat.matcher(nombre);
		if(mat.find()==false){
			throw new RazonSocialSoloPermiteLetrasException();
		 }	
    }

    

    
    
}
