package mercado.cliente;

import mercado.mercado.Contrato;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import mercado.excepciones.*;
import mercado.facturacion.*;


public abstract class Cliente {
    
    private String nombre;
    private BigDecimal dniocuit;
	private String domicilio;
	private String provincia;
	private String email;
	private String numeroTelefonoCompleto;
	private Integer codigodeArea;
	private Long telefono;
	private Integer numdeCliente;
	private ArrayList<Contrato> contratosCliente;
	private ArrayList<Factura> facturasCliente;
	
	
	protected Cliente(String nombre, BigDecimal dniocuit, String domicilio,String provincia, String email,Integer codigodeArea, Long telefono) {
		comprobarNombre(nombre);
		this.nombre = nombre.toUpperCase();
		this.dniocuit = dniocuit;
		this.domicilio = domicilio.toUpperCase();
		this.provincia = provincia.toUpperCase();
		comprobarEmail(email);
		this.email = email.toUpperCase();
		comprobarNumeroTelefono(codigodeArea,telefono);
		this.codigodeArea = codigodeArea;
		this.telefono = telefono;
		contratosCliente= new ArrayList<Contrato>();
		facturasCliente= new ArrayList<Factura>();
    }
    
    //SETTERS

    public void setNombre(String nombre) {
		comprobarNombre(nombre);
		this.nombre = nombre.toUpperCase();
    }
    
    public void setDomicilio(String domicilio) {
		this.domicilio = domicilio.toUpperCase();
    }
    
    public void setEmail(String email) {
		comprobarEmail(email);
        this.email = email.toUpperCase();
	}

    public void setTelefono(Long telefono) {
		this.telefono = telefono;
    }
    
    public void setProvincia(String provincia) {
		this.provincia = provincia.toUpperCase();
    }
    
    public void setDniocuit(BigDecimal dniocuit) {
		this.dniocuit = dniocuit;		
    }
	
	public void setNumdeCliente(Integer numdeCliente) {
		this.numdeCliente = numdeCliente;
	}

	public void setCodigodeArea(Integer codigodeArea) {
		this.codigodeArea = codigodeArea;
	}


	public void setContratosCliente(ArrayList<Contrato> contratosCliente) {
		this.contratosCliente = contratosCliente;
	}

	public void setFacturasCliente(ArrayList<Factura> facturasCliente) {
		this.facturasCliente = facturasCliente;
	}

	//GETTERS
	
	public String getNombre() {
		return nombre;
	}
	
	public String getDomicilio() {
		return domicilio;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getProvincia() {
		return provincia;
	}

	public Long getTelefono() {
		return telefono;
	}

	public BigDecimal getDniocuit() {
		return dniocuit;
	}

	public Integer getNumdeCliente() {
		return numdeCliente;
	}

	public Integer getCodigodeArea() {
		return codigodeArea;
	}

	public String getNumeroTelefonoCompleto() {
		return numeroTelefonoCompleto;
	}	

	public ArrayList<Contrato> getContratosCliente() {
		return contratosCliente;
	}

	public ArrayList<Factura> getFacturasCliente() {
		return facturasCliente;
	}


	//OTROS METODOS

	//COMPROBACIONES

	private void comprobarEmail(String email){
		Pattern pat = Pattern.compile("^([\\w-])*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher mat = pat.matcher(email);
		if(mat.find()==false){
			throw new EmailInvalidoException();
		 }
	}
	
	private void comprobarNumeroTelefono(Integer codigodeA,Long telefon){
		String aux=codigodeA.toString()+telefon.toString();
		Pattern pat = Pattern.compile("^([0-9]{10})$");
		Matcher mat = pat.matcher(aux);
		if(mat.find()==false){
			throw new NumeroTelefonoNoValidoExeption();
		 }
	}

	private void comprobarNombre(String nombre){
		Pattern pat = Pattern.compile("^([a-zA-Z\s\s]{0,40})$");
		Matcher mat = pat.matcher(nombre);
		if(mat.find()==false){
			throw new NombreSoloPermiteLetrasException();
		 }	
	}

	protected abstract void comprobarDniOcuit(BigDecimal dniocuit);

	//METODOS CONTRATOS
	public void agregarContratoaCliente(Contrato contrato){
		contratosCliente.add(contrato);
	}
	public void modificarContrato(Contrato contrato, Contrato modificado) {
        contratosCliente.remove(contrato);
        contratosCliente.add(modificado);
	}
	
	public Contrato buscarContratoxNumero(Integer numContrato) {
        Contrato contratoEncontrado = null;
        for (Contrato aux : contratosCliente) {
            if (aux.getCodigoConcepto().equals(numContrato)) {
                contratoEncontrado = aux;
                break;
            }
        }
        if (contratoEncontrado == null) {
            throw new ContratoNoEncontradoException();
        }
        return contratoEncontrado;
    }


	//METODOS FACTURAS

	public void agregarFacturasaCliente(Factura factura){
		facturasCliente.add(factura);
	}
	

}
