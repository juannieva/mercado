package mercado.facturacion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;

import mercado.cliente.Cliente;
import mercado.excepciones.*;

public class LibroFacturas {
    private ArrayList<Factura> facturas;
    private ArrayList<Cliente> clientes;

    public LibroFacturas(ArrayList<Cliente> clientes){
        this.clientes=clientes;
        facturas = new ArrayList<Factura>();
    }

    public ArrayList<Factura> getFacturas() {
        return facturas;
    }


    public Integer getCantidadFacturas(){
        return facturas.size();
    }

    public void agregarFactura(Factura factura) {
            for (Factura aux : facturas) {
                if (aux.equals(factura)) {
                  throw new FacturaExisteException("Factura ya Existe");
                }  
            }
        factura.setNroFactura(facturas.size()+1);
        getCliente(factura.getCliente()).agregarFacturasaCliente(factura);
        facturas.add(factura);
    }


    public Factura encontrarFactura(Integer nroFactura){
        Factura facturaEncontrada = null;
        for (Factura aux : facturas) {
            if (aux.getNroFactura().equals(nroFactura)){
                facturaEncontrada = aux;
                break;
            }
        }
        if (facturaEncontrada ==null){
            throw new FacturaNoExisteException();
        }
        return facturaEncontrada;
    }


    public void eliminarFactura(Integer numeroFactura){
        Factura facturaAEliminar=encontrarFactura(numeroFactura);
        facturas.remove(facturaAEliminar);
    }

    public void modificarFactura(Factura factura, Factura nuevaFactura){
        eliminarFactura(factura.getNroFactura());
        agregarFactura(nuevaFactura);
    }

    public ArrayList<Factura> buscarFacturasDeUnMes(Integer numeroDeMes){
        ArrayList<Factura> facturasEncontradas = new ArrayList<Factura>();
        for (Factura aux : facturas) {
            if (aux.getFechaFacturacion().getMonthValue() == numeroDeMes){
                facturasEncontradas.add(aux);
            }           
        }
        if(facturasEncontradas.isEmpty()){
            throw new FacturasNoEncontradasException("Factura no Encontrada");
        }
        return facturasEncontradas;    

    }


    public BigDecimal calcularImporteDeFacturasDeUnMes(Integer numeroDeMes){
        ArrayList<Factura> facturasEncontradas = buscarFacturasDeUnMes(numeroDeMes);
        BigDecimal importeTotal = new BigDecimal (0);
        for (Factura aux : facturasEncontradas){
            importeTotal = importeTotal.add(aux.getPrecioTotalFactua().setScale(0, RoundingMode.HALF_UP));
        }
        return importeTotal;
    }

    public ArrayList<Factura> encontrarFacturasEnUnPeriodo(LocalDate fecha1,LocalDate fecha2){
        ArrayList<Factura> facturasEncontradas = new ArrayList<Factura>();
        for (Factura auxiliar : facturas) {
            if ( fecha1.isBefore(auxiliar.getFechaFacturacion()) &&  fecha2.isAfter(auxiliar.getFechaFacturacion())){
                facturasEncontradas.add(auxiliar);
            }
        }
       if (facturasEncontradas.isEmpty()) {
            throw new FacturasNoEncontradasException("No existe factura en este periodo");
        }
        return facturasEncontradas;
    }

    public Cliente getCliente(Cliente cliente){
        Cliente clienteEncontrado=null;
        for(Cliente aux: clientes){
            if(aux.equals(cliente)){
                clienteEncontrado=aux;
                break;
            }
        }
        return clienteEncontrado;
    }
}