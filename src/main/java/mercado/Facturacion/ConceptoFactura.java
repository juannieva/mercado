package mercado.facturacion;

import java.math.BigDecimal;

public interface ConceptoFactura {
    public Integer getCodigoConcepto();
    public BigDecimal getImporteMensual();
    
}
