package mercado.facturacion;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import mercado.cliente.Cliente;
import mercado.excepciones.ConceptoYaCargadoException;
import mercado.mercado.ResponsableMercado;


public class Factura {
    private Cliente cliente;
    private Integer nroFactura;
    private LocalDate fechaFacturacion;
    private String descripcion;
    private ArrayList<ConceptoFactura> conceptos;
    private ResponsableMercado responsableMercado;

    //CONTRUCTOR

    public Factura(LocalDate fechaFacturacion, String descripcion, Cliente cliente) {
        this.fechaFacturacion = fechaFacturacion;
        this.descripcion = descripcion;
        this.cliente=cliente;
        conceptos=new ArrayList<ConceptoFactura>();
    }

    //SETTERS

    public void setNroFactura(Integer nroFactura) {
        this.nroFactura = nroFactura;
    }

    public void setFechaFacturacion(LocalDate fechaFacturacion) {
        this.fechaFacturacion = fechaFacturacion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setResponsableMercado(ResponsableMercado responsableMercado){
        this.responsableMercado=responsableMercado;
    }
    

    //GETTERS 

    public Integer getNroFactura() {
        return nroFactura;
    }

    public LocalDate getFechaFacturacion() {
        return fechaFacturacion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    

    public Cliente getCliente() {
        return cliente;
    }

    public ResponsableMercado getResponsableMercado(){
        return responsableMercado;
    }

    
    //OTROS METODOS


    public BigDecimal getPrecioTotalFactua(){
        BigDecimal precioTotalFactura=new BigDecimal("0");
        for(ConceptoFactura aux: conceptos){
            precioTotalFactura=precioTotalFactura.add(aux.getImporteMensual());
    }
    return precioTotalFactura;
    
    }
    public void agregarConcepto(ConceptoFactura concepto){
        for(ConceptoFactura var : conceptos) {
            if (var.getCodigoConcepto().equals(concepto.getCodigoConcepto())) {
                throw new ConceptoYaCargadoException("Concepto ya Cargado en Factura");
            }
        }
        conceptos.add(concepto);
    }

   
   
	
}
