package mercado.excepciones;

public class LecturaNoExisteException extends RuntimeException{
    public LecturaNoExisteException(String msj){super(msj);}
}
