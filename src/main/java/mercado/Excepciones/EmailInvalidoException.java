package mercado.excepciones;

public class EmailInvalidoException extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public EmailInvalidoException(){
        super("Email no valido ingresado");
    }
    
}
