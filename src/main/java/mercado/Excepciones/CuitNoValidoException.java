package mercado.excepciones;

public class CuitNoValidoException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public CuitNoValidoException(){
        super("Cuit no valido Ingresado");
    }
}
