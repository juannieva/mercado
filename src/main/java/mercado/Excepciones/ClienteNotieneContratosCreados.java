package mercado.excepciones;

public class ClienteNotieneContratosCreados extends RuntimeException{
     /**
     *
     */
    private static final long serialVersionUID = 1L;
    public ClienteNotieneContratosCreados(){
        super("Cliente no tiene contratos creados");
    }
}
