package mercado.excepciones;

public class ConceptoYaCargadoException extends RuntimeException {
    public ConceptoYaCargadoException(String msj) {
        super(msj);
    }
}
