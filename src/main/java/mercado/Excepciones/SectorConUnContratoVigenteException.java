package mercado.excepciones;

public class SectorConUnContratoVigenteException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public SectorConUnContratoVigenteException() {
        super("Error al eliminar  porque  el sector tiene un contrato Vigente");
    }
    
}
