package mercado.excepciones;

public class NumeroTelefonoNoValidoExeption extends RuntimeException {
     /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NumeroTelefonoNoValidoExeption(){
        super("Numero ingresado no valido");
    }
    
}
