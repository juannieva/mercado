package mercado.excepciones;

public class ClienteDuplicadoException extends RuntimeException {

    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ClienteDuplicadoException() {
        super("Cliente ya fue Registrado en el Sistema");
    }    
}
