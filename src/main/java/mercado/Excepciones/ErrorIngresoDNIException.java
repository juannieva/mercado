package mercado.excepciones;

public class ErrorIngresoDNIException extends RuntimeException {
    public ErrorIngresoDNIException(String msj) {
        super(msj);
    }
}
