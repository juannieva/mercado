package mercado.excepciones;

public class ClienteNoEncontradoException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    public ClienteNoEncontradoException(){
        super("Cliente no Encontrado");
    }
    
}
