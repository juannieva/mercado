package mercado.excepciones;

public class SectorNoEncontradoException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    public SectorNoEncontradoException(){
        super("Sector no encontrado");
    }
}
