package mercado.excepciones;

public class ApellidoSoloPermiteLetrasException extends RuntimeException {
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ApellidoSoloPermiteLetrasException() {
        super("Apellido solo se permiten letras");
    }
}
