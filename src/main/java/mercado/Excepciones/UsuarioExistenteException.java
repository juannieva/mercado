package mercado.excepciones;

public class UsuarioExistenteException extends RuntimeException {
    public UsuarioExistenteException(String msj) {
        super(msj);
    }
}
