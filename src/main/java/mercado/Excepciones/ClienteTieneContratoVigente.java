package mercado.excepciones;

public class ClienteTieneContratoVigente extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public ClienteTieneContratoVigente(){
        super("Cliente tiene un contrato todavia vigente");
    }
    
}
