package mercado.excepciones;

public class MenordeEdadException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = -44598869027767763L;

    public MenordeEdadException(){
        super("Menor de 18 años no permitido");
    }    
}
