package mercado.excepciones;

public class NombreSoloPermiteLetrasException extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NombreSoloPermiteLetrasException() {
        super("En el campo nombre solo se permiten Letras");
    }
}
