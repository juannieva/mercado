package mercado.excepciones;

public class UbicacionExistenteException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    public UbicacionExistenteException(){
        super("Ubicacion de Sector ya Existe en el Mercado");
    }
}
