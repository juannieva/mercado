package mercado.excepciones;

public class UsuarioInexistenteException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public UsuarioInexistenteException(String msj) {
        super(msj);
    }
}
