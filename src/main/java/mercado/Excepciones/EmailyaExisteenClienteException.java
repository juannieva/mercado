package mercado.excepciones;

public class EmailyaExisteenClienteException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public EmailyaExisteenClienteException() {
        super("Email ya fue registrado en un Cliente");
    }
    
}
