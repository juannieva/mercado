package mercado.excepciones;

public class PuestoYaAsociadoaContratoException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public PuestoYaAsociadoaContratoException() {
        super("Puesto asociado a contrato");
    }
    
}
