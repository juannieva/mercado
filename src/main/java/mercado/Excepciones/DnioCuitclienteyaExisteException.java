package mercado.excepciones;

public class DnioCuitclienteyaExisteException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DnioCuitclienteyaExisteException() {
        super("Dni o Cuit ya fue registrado en un Cliente");
    }
    
}
