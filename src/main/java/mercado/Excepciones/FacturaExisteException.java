package mercado.excepciones;

public class FacturaExisteException extends RuntimeException {
    public FacturaExisteException(String msj) {
        super(msj);
    }
}
