package mercado.excepciones;

public class PuestoNoEncontradoException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public PuestoNoEncontradoException() {
        super("El numero de puesto no encontrado en el sector");
    }
    
}
