package mercado.excepciones;

public class ContratoNoEncontradoException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ContratoNoEncontradoException() {
        super("Numero de Contrato No encontrado");
    }
    
}
