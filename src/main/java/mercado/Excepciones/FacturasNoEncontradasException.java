package mercado.excepciones;

public class FacturasNoEncontradasException extends RuntimeException {
    public FacturasNoEncontradasException(String msj) {
        super(msj);
    }
}
