package mercado.excepciones;

public class NombreyaExisteException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    public NombreyaExisteException(){
        super("Nombre ya fue registrado en un Cliente");
    }
}
