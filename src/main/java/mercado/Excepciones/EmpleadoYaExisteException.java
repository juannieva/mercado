package mercado.excepciones;

public class EmpleadoYaExisteException extends RuntimeException {
    public EmpleadoYaExisteException(String msj) {
        super(msj);
    }
}
