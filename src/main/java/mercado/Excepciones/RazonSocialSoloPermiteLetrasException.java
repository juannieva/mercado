package mercado.excepciones;

public class RazonSocialSoloPermiteLetrasException extends RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public RazonSocialSoloPermiteLetrasException() {
        super("En el campo Razon Social solo se permiten Letras");
    }
}
