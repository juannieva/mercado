package mercado.excepciones;

public class FacturaNoExisteException extends RuntimeException{
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public FacturaNoExisteException() {
        super("Numero de Factura no encontrado");
    }
}
