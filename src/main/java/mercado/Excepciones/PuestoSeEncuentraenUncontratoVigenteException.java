package mercado.excepciones;

public class PuestoSeEncuentraenUncontratoVigenteException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public PuestoSeEncuentraenUncontratoVigenteException() {
        super("Puesto seleccionado ya se encuentra en un contrato Vigente");
    }
    
}
